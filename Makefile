# -*- coding: utf-8 -*-

.PHONY: serve
serve: www/bundle.js www/index.html
	@echo "Please visit http://localhost:8000 with a browser"
	cd www && python -m http.server

# Compile python sources and bundle them up when any python file changes
www/bundle.js: pyvuetodo/*.py
	@echo "Transpiling python to javascript..."
	pj -o build pyvuetodo
	@echo "Bundling javascript..."
	node_modules/.bin/webpack

# Copy html and css files when they change
www/%.html: pyvuetodo/%.html
	@echo "Copying static html file"
	cp $< $@

www/%.css: pyvuetodo/%.css
	@echo "Copying static css file"
	cp $< $@
