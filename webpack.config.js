// -*- coding: utf-8 -*-

var path = require('path');

module.exports = {
    entry: __dirname + "/build/pyvuetodo.js",
    output: {
        path: __dirname + "/www",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015'],
                    plugins: ['transform-runtime']
                }
            }
        ],
        rules: [
          {
            test: /\.js$/,
            enforce: "pre",
            loader: "source-map-loader"
          }
        ]
    },
    resolve: {
        alias: {
            'vue': __dirname +'/node_modules/vue/dist/vue.js'
        }
    },
    devtool: "#source-map"
};
