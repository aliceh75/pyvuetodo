# -*- coding: utf-8 -*-
#
# Using Vue reactive framework in Python with a Python to javascript transpiler.
#

import vue as Vue


# Create an event bus for app wide comunication. $ is not
# valid in python function names, so create shortcuts.
event_hub = Vue()
event_hub_emit = getattr(event_hub, '$emit').bind(event_hub)
event_hub_on = getattr(event_hub, '$on').bind(event_hub)


# Handler on single todo item when checkbox is clicked.
# `self` automatically bind to `this`.
def todo_clicked(self, event):
    event_hub_emit('todo-done', self.todo)


# Declare our todo-item component
Vue.component('todo-item', {
    'props': ['todo'],
    'template': '<div><input type="checkbox" @click="clicked" :checked="false"  /><label>{{ todo }}</label></div>',
    'methods': {'clicked': todo_clicked}
})


# Declare our todo-list component
Vue.component('todo-list', {
    'props': ['todos'],
    'template': '<ul><li v-for="todo in todos"><todo-item :todo="todo" /></li></ul>'
})


# Handler to add new todos.
def add_todo(self):
    value = self.new_todo.trim()
    if not value:
        return

    self.todos.push(value)
    self.new_todo = ''


# Handler invoked when a 'todo-done' event is emitted
def todo_done(self, todo):
    index = self.todos.indexOf(todo)
    if index >= 0:
        self.todos.splice(index, 1)


# Our application
vm = Vue({
    'el': '#app',
    'template': (
        '<div>'
        '<input v-model="new_todo" @keyup.enter="add_todo" />'
        '<todo-list :todos=todos />'
        '</div>'
    ),
    'data': {
        'todo_id': 0,
        'todos': [],
        'new_todo': ''
    },
    'methods': {
        'add_todo': add_todo,
        'todo_done': todo_done
    }
})


event_hub_on('todo-done', todo_done.bind(vm))
